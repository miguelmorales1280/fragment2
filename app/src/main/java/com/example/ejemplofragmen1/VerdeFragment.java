package com.example.ejemplofragmen1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

public class VerdeFragment extends Fragment {
    // Método
    public VerdeFragment(){

    }

    // Método
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState){
        return inflater.inflate(R.layout.fragment_verde, container, false);

    }
}

